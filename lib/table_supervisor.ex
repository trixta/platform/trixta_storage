defmodule TrixtaStorage.TableSupervisor do
  @moduledoc """
  A supervisor that starts `TableServer` processes dynamically
  as storage tables are opened through TrixtaStorage.
  """

  use DynamicSupervisor

  require Logger

  alias TrixtaStorage.TableServer

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts a `TableServer` process given a table name and supervises it.
  """
  def start_table_server(table_name, table_options) do
    child_spec = %{
      id: TableServer,
      start: {TableServer, :start_link, [table_name, table_options]},
      restart: :transient
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Stop the `TableServer` process normally. It CAN be restarted.
  """
  def stop_table_server(table_name) do
    with child_pid when not is_nil(child_pid) <- TableServer.table_pid(table_name) do
      Logger.info("Stopping table server process for table named '#{table_name}'.")
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
    end
  end

  def stop_table_server(table_name) do
    stop_table_server(table_name)
  end

  def stop_all_table_servers() do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.each(fn {_, child_pid, _, _} ->
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
    end)
  end

  def list_table_servers() do
    :global.registered_names()
    |> Enum.filter(fn {module, _name} -> module == TrixtaStorage.TableRegistry end)
    |> Enum.reduce([], fn {_, name}, names -> [name | names] end)
    |> Enum.sort()
  end
end
