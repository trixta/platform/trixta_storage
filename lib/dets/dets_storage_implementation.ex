defmodule TrixtaStorage.Impl.Dets do
  @moduledoc """
  Dets implementation of the trixta_storage interface.

  Dets-specific features include using the native match_object/2 function
  to look up on a key by prefix, e.g. 'eth_contract_watcher:*'
  """

  @behaviour TrixtaStorage.Backend
  alias TrixtaUtils.SystemUtils
  alias TrixtaStorage.TableServer
  require Logger

  # for keys like 'eth_contract_watcher:abc123'
  @key_token_separator ":"
  # for prefix-based lookup, e.g. 'eth_contract_watcher:*'
  @query_wildcard_symbol "*"

  def open_table(table_name) do
    case :dets.open_file(
           table_name,
           [
             {:file, '#{file_path(table_name)}'},
             {:cache_size, 1000}
           ]
         ) do
      {:ok, table_ref} ->
        Logger.info(
          "Dets table opened successfully with name #{table_name} and file path: #{
            inspect(file_path(table_name))
          }"
        )

        {:ok, table_ref}

      err ->
        Logger.error(
          "Could not open dets table with name #{table_name} . Error from :dets.open_file : #{
            inspect(err)
          }"
        )

        err
    end
  end

  def close_table(table_name) do
    case :dets.close(table_name) do
      :ok ->
        Logger.info("Successfully closed dets table named #{table_name}")
        :ok

      err ->
        Logger.error(
          "Could not close dets table named #{table_name} . :dets.close returned error: #{
            inspect(err)
          }"
        )

        err
    end
  end

  def delete_table(table_name) do
    close_table(table_name)
    # Dets has no function for deleting tables, so we just delete the file itself:
    case File.rm(file_path(table_name)) do
      :ok ->
        Logger.info("Dets table file deleted: #{table_name} .")
        :ok

      err ->
        Logger.error("Cannot delete file for dets table #{table_name} . Error: #{inspect(err)}")
    end
  end

  def table_exists?(table_name) do
    # In dets, a table exists if the underlying file exists.
    # This code assumes the file is a valid dets table and is not corrupted.
    # Not checking that here.
    file_path(table_name) |> File.exists?()
  end

  defp file_path(table_name) do
    SystemUtils.system_path() <>
      Enum.join(
        [
          SystemUtils.environment(:trixta_storage),
          SystemUtils.trixta_space_port(),
          Atom.to_string(table_name)
        ],
        "_"
      )
  end

  # Used to 'glue' two functions together into a new function.
  # Allows us to tweak the arguments that dets passes to our traverse callback,
  # before we pass them on to the caller's callback function.
  defp compose(f, g), do: fn x -> g.(f.(x)) end

  def traverse(table_name, item_callback_function) do
    # The key might be stored as a tuple with 3 elements:
    # whole key, prefix, rest of key (to facilitate wildcard lookups).
    #
    # For convenience, we pass along only the first element of the tuple as the key, i.e. the 'whole' key.
    # This is fine because all TrixtaStorage functions (lookup, save, etc) understand this convention
    # and will convert appropriately.
    key_from_record = fn record ->
      case record do
        {whole_key, _prefix, _rest_of_key, value} ->
          {whole_key, value}

        {key, value} ->
          {key, value}

        value ->
          value
      end
    end

    :dets.traverse(table_name, compose(key_from_record, item_callback_function))
  end

  def save(table_name, key, value) do
    case :dets.info(table_name) do
      :undefined ->
        {:error, :table_not_found}

      _ ->
        table_options = TableServer.table_options(table_name)
        :dets.insert(table_name, dets_tuple_for_upsert(key, value, table_options))
    end
  end

  @doc """
  Returns all KV pairs from dets where the key matches the given pattern.
  The query can be limited to the first token in the key by using the @query_wildcard_symbol.

  ## Examples

     iex> TrixtaStorage.lookup(:space_123, "eth_contract_watcher:*")
      [
        {"eth_contract_watcher:123", %{"val" => "abc"}},
        {"eth_contract_watcher:456", %{"val" => "def"}}
      ]
  """
  def lookup(table_name, key_pattern, prefix_lookup_enabled \\ false) do
    if prefix_lookup_enabled do
      # If this table has prefix lookup enabled, we query using dets' match function
      # because it allows for wildcard searches on a tuple, for e.g. to query by prefix.

      # We can't enable this by default for all tables, because dets sometimes raises a 'premature_eof' error
      # when doing tuple-based wildcard lookups on large documents, e.g. trixta_roles that have lots of contract actions.
      # We don't need prefix-based lookup on roles, so we'd like to disable it for those tables, for e.g.
      :dets.match_object(table_name, dets_tuple_for_query(key_pattern, prefix_lookup_enabled))

    else
      # Prefix lookup capability is not enabled on this table, so do a standard dets lookup on the whole key.
      :dets.lookup(table_name, key_pattern)
    end
    |> format_query_results()
  end

  @doc """
  Deletes all objects matching the given key pattern.
  The query can be limited to the first token in the key by using the @query_wildcard_symbol.
  """
  def delete(table_name, key_pattern, prefix_lookup_enabled \\ false) do
    if prefix_lookup_enabled do
      # If prefix lookup is enabled for this table, we explode the key into a tuple
      # to enable wildcard-based lookup on the prefix.
      :dets.match_delete(table_name, dets_tuple_for_query(key_pattern, prefix_lookup_enabled))
    else
      # Prefix lookup is not enabled on this table, so just do a normal delete using the exact key.
      :dets.delete(table_name, key_pattern)
    end
  end

  @doc """
  Deletes all objects from the given table.
  """
  def delete_all(table_name) do
    :dets.delete_all_objects(table_name)
  end

  # Formats the results of :dets.match_object into a list of tuples looking like [{"whole_key", value}]
  defp format_query_results(dets_match_result) when is_list(dets_match_result) do
    dets_match_result
    |> Enum.map(fn item ->
      {item |> elem(0), item |> Tuple.to_list() |> List.last()}
    end)
  end

  defp format_query_results(_) do
    []
  end

  # The following functions are used to facilitate key lookup by prefix.
  # Keys are tokenized and converted into tuples with wildcard symbols
  # understood by dets' native match functions.

  defp dets_tuple_for_upsert(key, value, %{:prefix_lookup_enabled => true}) do
    # Prefix lookup is enabled for this table, so we explode the key into a tuple
    # with the prefix isolated into its own element, so we can query on it specifically later.

    # We can't enable this by default for all tables, because dets sometimes raises a 'premature_eof' error
    # when doing tuple-based wildcard lookups on large documents, e.g. trixta_roles that have lots of contract actions.
    # We don't need prefix-based lookup on roles, so we'd like to disable it for those tables, for e.g.
    key_to_dets_tuple(key)
    |> Tuple.append(value)
  end

  defp dets_tuple_for_upsert(key, value, _table_options) do
    # Prefix lookup is not enabled, so we store the exact whole key only.
    # No need to separate it into prefix and suffix
    {key, value}
  end

  defp dets_tuple_for_query(key_pattern, true) do
    # Prefix lookup is enabled for this table, so we explode the key into a tuple
    # with wildcards so we can query by prefix, for example.

    # We can't enable this by default for all tables, because dets sometimes raises a 'premature_eof' error
    # when doing tuple-based wildcard lookups on large documents, e.g. trixta_roles that have lots of contract actions.
    # We don't need prefix-based lookup on roles, so we'd like to disable it for those tables, for e.g.
    key_to_dets_tuple(key_pattern)
    |> replace_wildcards_with_underscore()
    # The last element in the tuple is the value. We don't care what it is.
    |> Tuple.append(:_)
  end

  defp dets_tuple_for_query(key, _false) do
    # Prefix lookup is not enabled, so we query by exact key.
    # No need to separate it into prefix and suffix
    key
  end

  # Generates a tuple of which the first element is the whole key,
  #
  # second element is the first token of the key,
  # third element is the rest of the key,
  #
  # This allows us to use dets' 'match_object' function to query by the first token or the whole key,
  # because all tuples have a guaranteed length of 4.
  defp key_to_dets_tuple(key) when is_binary(key) do
    [first_token | rest_of_tokens] = String.split(key, @key_token_separator)

    if String.contains?(key, @query_wildcard_symbol) do
      # The query contains a wildcard symbol, so we don't want to include
      # the exact key in the query.
      [:_, first_token, Enum.join(rest_of_tokens, @key_token_separator)]
    else
      # The query contains no wildcard symbols, so assume we want to match on the whole exact key:
      [key, first_token, Enum.join(rest_of_tokens, @key_token_separator)]
    end
    |> List.to_tuple()
  end

  defp key_to_dets_tuple(key) do
    {key}
  end

  # When we're querying, we want @query_wildcard_symbol to match anything.
  # Replace all wildcard characters with :_ which the dets match functions understand.
  defp replace_wildcards_with_underscore(tuple) when is_tuple(tuple) do
    tuple
    |> Tuple.to_list()
    |> Enum.map(fn token ->
      case token do
        @query_wildcard_symbol -> :_
        _ -> token
      end
    end)
    |> List.to_tuple()
  end

  defp get_table_options(table_name) do
    TrixtaServer.table_pid(table_name)
    |> GenServer.call(:table_options)
  end
end
