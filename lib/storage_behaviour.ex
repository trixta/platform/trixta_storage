defmodule TrixtaStorage.Backend do
  @moduledoc """
  Defines a behaviour that all storage implementations must adhere to.
  """

  @callback open_table(table_name :: term, table_options :: term) ::
              {:ok, table_ref :: term} | {:error, reason :: term}
  @callback close_table(table_name :: term) :: :ok | {:error, reason :: term}
  @callback delete_table(table_name :: term) :: :ok | {:error, reason :: term}
  @callback table_exists?(table_name :: term) :: bool
  @callback lookup(table_name :: term, key :: term, prefix_lookup_enabled :: term) :: value :: term
  @callback traverse(table_name :: term, item_callback :: function) :: term
  @callback save(table_name :: term, key :: term, value :: term) :: :ok | {:error, reason :: term}
  @callback delete(table_name :: term, key_pattern :: term, prefix_lookup_enabled :: term) :: :ok | {:error, reason :: term}
  @callback delete_all(table_name :: term) :: :ok | {:error, reason :: term}
end
