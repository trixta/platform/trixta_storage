defmodule TrixtaStorage do
  @moduledoc """
  Public interface for persistent storage within trixta.
  Custom storage backends can be added by implementing the TrixtaStorage.Backend behaviour.
  The backend module to use can be configured in app config under :trixta_storage -> :implementation_module
  The default is dets (implemented in lib/dets/dets_storage_implementation.ex)
  """

  alias TrixtaStorage.TableSupervisor

  @default_table_options %{}

  @doc """

  Starts a GenServer process for the given table name with the given options.
  Each storage implementation can decide what 'open table' means, e.g. for dets it will be
  :dets.open_file .
  Regardless, each logical table gets a genserver which stores the table's name and options.

   Returns all KV pairs from dets where the key matches the given pattern.
  The query can be limited to the first token in the key by using the @query_wildcard_symbol.

  ## Parameters
    - table_name: A system-wide unique name for this table.
    - table_options: Various settings around how data in this table should be treated, e.g. whether it should be indexed.
  """
  def open_table(table_name, table_options \\ @default_table_options)
      when is_map(table_options) do
    TableSupervisor.start_table_server(table_name, table_options)
  end

  def open_table(_app_name, table_name, table_options) do
    # We don't use the app_name to identify tables anymore,
    # but some callers may still pass in their table name until we get around to updating all repos.
    open_table(table_name, table_options)
  end

  def open_table(_app_name, table_name) do
    # We don't use the app_name to identify tables anymore,
    # but some callers may still pass in their table name until we get around to updating all repos.
    open_table(table_name)
  end

  def close_table(table_name) do
    TableSupervisor.stop_table_server(table_name)
  end

  def close_table(_app_name, table_name) do
    # We don't use the app_name to identify tables anymore,
    # but some callers may still pass in their table name until we get around to updating all repos.
    close_table(table_name)
  end

  @doc """
  Deletes a table entirely as opposed to just closing it.
  We need this because some entities now have their own table
  that shouldn't stick around if the entity is terminated.
  """
  def delete_table(table_name) do
    TrixtaStorage.Helpers.storage_implementation().delete_table(table_name)
  end

  @doc """
  Returns true if a table exists, even if it is currently closed.
  """
  def table_exists?(table_name) do
    TrixtaStorage.Helpers.storage_implementation().table_exists?(table_name)
  end

  def save(table_name, key, value) do
    TrixtaStorage.Helpers.storage_implementation().save(table_name, key, value)
  end

  def lookup(table_name, key, prefix_lookup_enabled \\ false) do
    TrixtaStorage.Helpers.storage_implementation().lookup(table_name, key, prefix_lookup_enabled)
  end

  def traverse(table_name, item_function) do
    TrixtaStorage.Helpers.storage_implementation().traverse(table_name, item_function)
  end

  def delete(table_name, key, prefix_lookup_enabled \\ false) do
    TrixtaStorage.Helpers.storage_implementation().delete(table_name, key, prefix_lookup_enabled)
  end

  def delete_all(table_name) do
    TrixtaStorage.Helpers.storage_implementation().delete_all(table_name)
  end

  # Returns the currently configured implementation module for trixta_storage from the app config.
  # Defaults to dets.
  defp storage_implementation() do
    Application.get_env(:trixta_storage, :implementation_module) || TrixtaStorage.Impl.Dets
  end
end
