defmodule TrixtaStorage.Helpers do
  # Returns the currently configured implementation module for trixta_storage from the app config.
  # Defaults to dets.
  def storage_implementation() do
    Application.get_env(:trixta_storage, :implementation_module) || TrixtaStorage.Impl.Dets
  end
end
