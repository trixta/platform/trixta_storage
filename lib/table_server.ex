defmodule TrixtaStorage.TableServer do
  @moduledoc """
  An instance of this server is started for every open table.
  It is responsible for trapping exits and closing its underlying storage resources gracefully.

  Idea taken from here: https://jbodah.github.io/blog/2017/05/18/implementing-graceful-exits-elixir/
  """

  use GenServer
  require Logger
  alias TrixtaStorage.Helpers

  # Client (Public) Interface

  @doc """
  Spawns a new table server process registered under the table name.

  Returns Space PID or :error.

  ## Examples

      # iex> TrixtaStorage.TableServer.start_link(:my_table_name, %{:prefix_lookup_enabled => true})
      # {:ok, #PID<0.310.0>,

      # iex> TrixtaStorage.TableServer.start_link(:my_table_name, %{:prefix_lookup_enabled => true})
      # {:error, {:already_started, #PID<0.393.0>}}

  """
  def start_link(table_name, table_options) do
    GenServer.start_link(
      __MODULE__,
      {table_name, table_options},
      name: via_tuple(table_name)
    )
  end

  @doc """
  Gets a tuple used to register and lookup a dets table server process by table name.

  Returns tuple.

  ## Examples

      # iex> TrixtaStorage.TableServer.via_tuple(:my_table_name)
      # {:via, :global,
      #  {TrixtaStorage.TableRegistry, :my_table_name}}

  """
  def via_tuple(table_name) do
    {:via, :global, {TrixtaStorage.TableRegistry, table_name}}
  end

  def table_options(table_name) do
    table_pid(table_name) |> GenServer.call(:table_options)
  end

  # Server Callbacks

  def init({table_name, table_options}) do
    # We need to trap exits so that terminate/2 can be called when this GenServer exits.
    # This allows us to properly close the dets table associated with this process.
    # A more robust solution would be for another process to monitor this one, but this is OK for now.
    Process.flag(:trap_exit, true)

    Logger.info("Opening trixta_storage table: #{table_name}")

    case Helpers.storage_implementation().open_table(table_name) do
      {:ok, table_ref} ->
        {:ok, {table_name, table_options}}

      err ->
        Logger.error(
          "Could not open trixta_storage table called #{table_name} . Error: #{inspect(err)}"
        )

        # We could not open the table, so don't keep this process around.
        # The start_link function will return {:error, :storage_table_init_error}
        {:stop, :storage_table_init_error}
    end
  end

  def handle_call(:table_options, _from, {table_name, table_options} = state) do
    {:reply, table_options, state}
  end

  def table_pid(table_name) do
    case table_name
         |> via_tuple()
         |> GenServer.whereis() do
      pid when is_pid(pid) ->
        pid

      _ ->
        if TrixtaStorage.table_exists?(table_name) do
          Logger.error(
            "TrixtaStorage: No TableServer running for table named #{table_name} . The underlying file or resource does exist, though. "
          )
        else
          Logger.error(
            "TrixtaStorage: No TableServer running for table named #{table_name} . The underlying file or resource does NOT exist."
          )
        end
    end
  end

  @doc """
  Called by the VM when the GenServer is about to exit.
  Used to properly close this space's KV dets table.
  Only this process can close the table because it has opened + owns it (inside the init function)
  """
  def terminate(_reason, {table_name, table_options} = _state) do
    cleanup(table_name)
  end

  def handle_info({:EXIT, _from, _reason}, {table_name, table_options} = state) do
    cleanup(table_name)
    {:noreply, state}
  end

  defp cleanup(table_name) do
    Logger.info(
      "Closing trixta_storage table #{table_name} because the table's GenServer is going down."
    )

    Helpers.storage_implementation().close_table(table_name)
  end
end
