defmodule TrixtaStorageTest.DetsTest do
  @moduledoc """
  Tests the basic TrixtaStorage plumbing with dets as the default implementation
  """
  use ExUnit.Case
  alias TrixtaStorage.{TableSupervisor, TableServer}
  doctest TrixtaStorage

  setup do
    TableSupervisor.stop_all_table_servers()

    {:ok, %{}}
  end

  test "dets table is opened" do
    TrixtaStorage.open_table(:test_app, :open_table_test)
    assert :open_table_test in :dets.all()
    assert TableSupervisor.list_table_servers() |> length() === 1
  end

  test "dets table can be closed manually" do
    TrixtaStorage.open_table(:close_table_test)
    assert :close_table_test in :dets.all()
    assert TableSupervisor.list_table_servers() |> length() === 1

    TrixtaStorage.close_table(:close_table_test)

    refute :close_table_test in :dets.all()
    assert TableSupervisor.list_table_servers() |> length() === 0
  end

  test "can write and lookup on dets table" do
    TrixtaStorage.open_table(:rw_test_table)
    TrixtaStorage.save(:rw_test_table, "key123", %{"value" => "def456"})
    assert TrixtaStorage.lookup(:rw_test_table, "key123") === [{"key123", %{"value" => "def456"}}]

    TrixtaStorage.save(:rw_test_table, "key123", %{"value" => "0000"})
    assert TrixtaStorage.lookup(:rw_test_table, "key123") === [{"key123", %{"value" => "0000"}}]

    assert TrixtaStorage.lookup(:rw_test_table, "non_existant_key") === []
  end

  test "Value can be retrieved by exact key" do
    TrixtaStorage.open_table(:test_table)
    TrixtaStorage.save(:test_table, "level1:level2:level3", %{"test_value" => "abc123"})

    assert TrixtaStorage.lookup(:test_table, "level1:level2:level3") == [
             {"level1:level2:level3", %{"test_value" => "abc123"}}
           ]
  end

  test "Value can be retrieved by first token" do
    TrixtaStorage.open_table(:test_table_2, %{:prefix_lookup_enabled => true})
    TrixtaStorage.save(:test_table_2, "level1:level2a:level3", %{"test_value" => "abc123"})
    TrixtaStorage.save(:test_table_2, "level1:level2b:level3", %{"test_value" => "abc123"})

    assert TrixtaStorage.lookup(:test_table_2, "level1:*", true) |> length() == 2
  end

  test "Value can be updated by key" do
    TrixtaStorage.open_table(:test_table_3)
    TrixtaStorage.save(:test_table_3, "upsert:test", %{"test_value" => "abc123"})

    assert TrixtaStorage.lookup(:test_table_3, "upsert:test") === [
             {"upsert:test", %{"test_value" => "abc123"}}
           ]

    TrixtaStorage.save(:test_table_3, "upsert:test", %{"test_value" => "def456"})

    assert TrixtaStorage.lookup(:test_table_3, "upsert:test") === [
             {"upsert:test", %{"test_value" => "def456"}}
           ]
  end

  test "can traverse dets table" do
    TrixtaStorage.open_table(:traverse_test_table)
    TrixtaStorage.save(:traverse_test_table, "key1", %{"value" => "123"})
    TrixtaStorage.save(:traverse_test_table, "key2", %{"value" => "456"})
    TrixtaStorage.save(:traverse_test_table, "key3", %{"value" => "789"})

    records =
      TrixtaStorage.traverse(:traverse_test_table, fn {key, value} ->
        {:continue, {key, value}}
      end)
      |> Enum.into(%{})

    assert Map.size(records) === 3
    assert records["key1"] === %{"value" => "123"}
    assert records["key2"] === %{"value" => "456"}
    assert records["key3"] === %{"value" => "789"}
  end

  test "can delete item by key" do
    TrixtaStorage.open_table(:delete_test_table, %{:prefix_lookup_enabled => true})
    TrixtaStorage.save(:delete_test_table, "level1:level2:level3", %{"value" => "123"})
    TrixtaStorage.save(:delete_test_table, "level1:level2b", %{"value" => "456"})
    TrixtaStorage.save(:delete_test_table, "key3", %{"value" => "789"})

    assert TrixtaStorage.lookup(:delete_test_table, "level1:*", true) |> length() === 2
    TrixtaStorage.delete(:delete_test_table, "level1:level2:level3", true)
    assert TrixtaStorage.lookup(:delete_test_table, "level1:*", true) |> length() === 1
    assert TrixtaStorage.lookup(:delete_test_table, "key3", true) |> length() === 1
  end

  test "can delete all objects in table" do
    TrixtaStorage.open_table(:delete_all_table, %{:prefix_lookup_enabled => true})
    TrixtaStorage.save(:delete_all_table, "level1:level2:level3", %{"value" => "123"})
    TrixtaStorage.save(:delete_all_table, "level1:level2b", %{"value" => "456"})
    TrixtaStorage.save(:delete_all_table, "key3", %{"value" => "789"})

    assert TrixtaStorage.lookup(:delete_all_table, "level1:*", true) |> length() === 2
    assert TrixtaStorage.lookup(:delete_all_table, "key3", true) |> length() === 1

    TrixtaStorage.delete_all(:delete_all_table)
    assert TrixtaStorage.lookup(:delete_all_table, "level1:*", true) |> length() === 0
    assert TrixtaStorage.lookup(:delete_all_table, "key3", true) |> length() === 0
  end

  test "non-existing table behaves properly" do
    assert TrixtaStorage.save(:non_existing_table, "key123", %{"value" => "def456"})
  end

  test "dets table is closed when its process exits normally" do
    {:ok, _table_ref} = TrixtaStorage.open_table(:exit_test_table)
    assert :exit_test_table in :dets.all()

    TableServer.table_pid(:exit_test_table)
    |> Process.exit(:normal)

    # Give the process some time to do cleanup
    :timer.sleep(150)

    refute :exit_test_table in :dets.all()
  end

  test "dets table is closed when its process receives :shutdown message" do
    # The :shutdown signal is sent to a process when it terminates because of a SIGTERM,
    # as would be the case when Kubernetes stops the pod.
    # We create a :shutdown signal here by terminating the dets table server at the supervisor level.
    {:ok, _table_ref} = TrixtaStorage.open_table(:sigterm_test_table)
    assert :sigterm_test_table in :dets.all()

    TableSupervisor.stop_table_server(:sigterm_test_table)

    # Give the process some time to do cleanup
    :timer.sleep(150)

    refute :sigterm_test_table in :dets.all()
  end

  # Ideally we'd have this enabled on all tables,
  # but there's an issue where dets raises a 'premature_eof' error
  # when traversing large records inserted in this manner with the key tokenized into a tuple.
  # So we disable it by default and individual tables can opt in through the table_options.
  test "Prefix-based lookup is disabled by default" do
    # We don't pass any table_options, so prefix_lookup_enabled should default to false.
    {:ok, _table_ref} = TrixtaStorage.open_table(:no_lookup_table)
    TrixtaStorage.save(:no_lookup_table, "prefix:suffix1", "val1")
    TrixtaStorage.save(:no_lookup_table, "prefix:suffix2", "val2")
    # Wildcard lookup returns nothing for tables where it's disabled
    assert TrixtaStorage.lookup(:no_lookup_table, "prefix:*") |> length() === 0

    # Try the same for delete:
    TrixtaStorage.delete(:no_lookup_table, "prefix:*")
    # Nothing should have been deleted because prefix lookup is disabled:
    assert TrixtaStorage.lookup(:no_lookup_table, "prefix:suffix1") === [
             {"prefix:suffix1", "val1"}
           ]

    assert TrixtaStorage.lookup(:no_lookup_table, "prefix:suffix2") === [
             {"prefix:suffix2", "val2"}
           ]

    # Deleting by the exact key still works:
    TrixtaStorage.delete(:no_lookup_table, "prefix:suffix1")
    TrixtaStorage.delete(:no_lookup_table, "prefix:suffix2")
    assert TrixtaStorage.lookup(:no_lookup_table, "prefix:suffix1") === []
    assert TrixtaStorage.lookup(:no_lookup_table, "prefix:suffix2") === []
  end

  test "Can delete table" do
    {:ok, _table_ref} = TrixtaStorage.open_table(:table_to_delete_1)
    {:ok, _table_ref} = TrixtaStorage.open_table(:table_to_delete_2)

    assert TrixtaStorage.table_exists?(:table_to_delete_1)
    assert TrixtaStorage.table_exists?(:table_to_delete_2)

    # Insert some values in both tables:
    TrixtaStorage.save(:table_to_delete_1, "key1", "val1")
    TrixtaStorage.save(:table_to_delete_2, "key2", "val2")

    # Delete one of the tables and confirm that it didn't affect the other table's data:
    TrixtaStorage.delete_table(:table_to_delete_1)
    refute TrixtaStorage.table_exists?(:table_to_delete_1)
    assert TrixtaStorage.table_exists?(:table_to_delete_2)
    assert TrixtaStorage.lookup(:table_to_delete_2, "key2") === [{"key2", "val2"}]

    # Everything ok. Delete the other table as well.
    TrixtaStorage.delete_table(:table_to_delete_2)
    refute TrixtaStorage.table_exists?(:table_to_delete_2)
  end
end
